import React, { Component } from 'react';

import Login from './pages/Login';
import CarList from './pages/CarList';
import Map from './pages/Map';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as userActions from './actions/user';

import { createAppContainer, createStackNavigator, createSwitchNavigator } from 'react-navigation';

const loggedRoutes = createStackNavigator({
  CarList: {
    name: 'CarList',
    screen: CarList,
  },
  Map: {
    name: 'Map',
    screen: Map,
  }
},
  {
    headerMode: 'none',
  }
);

const unloggedRoutes = createStackNavigator({
  Login: {
    name: 'Login',
    screen: Login,
  },
},
  {
    headerMode: 'none',
  }
);


class Routes extends Component {
  render() {
    const Routes = createAppContainer(
      createSwitchNavigator(
        {
          unloggedRoutes,
          loggedRoutes
        }, {
        initialRouteName: this.props.token ? 'loggedRoutes' : 'unloggedRoutes'
      }
      )
    );
    return (
      <Routes />
    );
  }
}


const mapStateToProps = state => {
  console.log(state);
  return ({
    token: state.user.token
  });
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(userActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Routes);